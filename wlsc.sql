-- MySQL dump 10.19  Distrib 10.3.32-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: wlsc
-- ------------------------------------------------------
-- Server version	10.1.48-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kabupaten`
--

DROP TABLE IF EXISTS `kabupaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kabupaten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kabupaten`
--

LOCK TABLES `kabupaten` WRITE;
/*!40000 ALTER TABLE `kabupaten` DISABLE KEYS */;
INSERT INTO `kabupaten` VALUES (1,'Bandar Lampung'),(5,'Lampung Tengah'),(3,'Mesuji'),(4,'Tanggamus'),(2,'Tulang Bawang Barat');
/*!40000 ALTER TABLE `kabupaten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `results` (
  `identity` varchar(50) NOT NULL,
  `scanResult` text,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_kabupaten` int(11) NOT NULL,
  PRIMARY KEY (`identity`),
  KEY `results_KABUPATEN_idx` (`id_kabupaten`),
  CONSTRAINT `results_KABUPATEN` FOREIGN KEY (`id_kabupaten`) REFERENCES `kabupaten` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
INSERT INTO `results` VALUES ('ITN Radar Site Cahyo Randu','[{\"address\":\"64:D1:54:95:E2:0F\",\"ssid\":\"SMD to LK\",\"channel\":\"5735/20/an\",\"signal\":\"-80\"},{\"address\":\"CC:2D:E0:03:11:72\",\"ssid\":\"CKL\",\"channel\":\"5745/20/an\",\"signal\":\"-78\"},{\"address\":\"B8:DD:71:C8:52:86\",\"ssid\":\"ZTE_5G_FRpdQ7\",\"channel\":\"5745/20-Ceee/ac\",\"signal\":\"-73\"},{\"address\":\"BA:DD:71:F8:52:86\",\"ssid\":\"\",\"channel\":\"5745/20-Ceee/ac\",\"signal\":\"-73\"},{\"address\":\"78:8A:20:98:CE:26\",\"ssid\":\"bdsy\",\"channel\":\"5760/20/an\",\"signal\":\"-89\"},{\"address\":\"48:8F:5A:18:D7:7D\",\"ssid\":\"RCM\",\"channel\":\"5765/20/an\",\"signal\":\"-84\"},{\"address\":\"CC:2D:E0:08:A4:B0\",\"ssid\":\"HMPTI\",\"channel\":\"5785/20/an\",\"signal\":\"-88\"},{\"address\":\"CC:2D:E0:91:E9:D2\",\"ssid\":\"\",\"channel\":\"5785/20/an\",\"signal\":\"-79\"},{\"address\":\"CC:2D:E0:40:E5:17\",\"ssid\":\"AP LHG PANCAKARSA\",\"channel\":\"5800/20/an\",\"signal\":\"-36\"},{\"address\":\"64:D1:54:A2:64:67\",\"ssid\":\"AP LHG BALI INDAH\",\"channel\":\"5805/20/an\",\"signal\":\"-42\"},{\"address\":\"48:8F:5A:FC:D3:D0\",\"ssid\":\"AP LHG BAKEM TENGAH\",\"channel\":\"5815/20/an\",\"signal\":\"-29\"},{\"address\":\"74:83:C2:58:AE:B7\",\"ssid\":\"\",\"channel\":\"5825/20/ac\",\"signal\":\"-75\"}]','2022-01-18 05:30:22',2),('ITN Radar Site Gisting','[{\"address\":\"4C:5E:0C:0F:B1:D9\",\"ssid\":\"WG-BEKRI\",\"channel\":\"5540/20-Ceee/ac\",\"signal\":\"-86\"},{\"address\":\"C4:AD:34:7E:CC:31\",\"ssid\":\"N7-Sect\",\"channel\":\"5620/20-Ceee/ac\",\"signal\":\"-88\"},{\"address\":\"74:4D:28:3E:FF:E3\",\"ssid\":\"\",\"channel\":\"5740/20/an\",\"signal\":\"-69\"},{\"address\":\"64:D1:54:AF:34:A5\",\"ssid\":\"\",\"channel\":\"5745/20/an\",\"signal\":\"-69\"},{\"address\":\"64:D1:54:AE:F9:E0\",\"ssid\":\"\",\"channel\":\"5745/20/an\",\"signal\":\"-81\"},{\"address\":\"64:D1:54:AF:2C:DD\",\"ssid\":\"\",\"channel\":\"5765/20/an\",\"signal\":\"-61\"},{\"address\":\"6C:3B:6B:42:B6:21\",\"ssid\":\"\",\"channel\":\"5785/20/an\",\"signal\":\"-41\"},{\"address\":\"B8:69:F4:70:CE:3F\",\"ssid\":\"\",\"channel\":\"5795/20/an\",\"signal\":\"-51\"},{\"address\":\"74:4D:28:E9:D4:22\",\"ssid\":\"\",\"channel\":\"5810/20/an\",\"signal\":\"-80\"},{\"address\":\"B8:69:F4:70:CD:BB\",\"ssid\":\"\",\"channel\":\"5825/20/an\",\"signal\":\"-54\"}]','2022-01-18 05:33:24',4),('ITN Radar Site Kalirejo','[{\"address\":\"2C:C8:1B:BA:A1:BF\",\"ssid\":\"AP-OSI\",\"channel\":\"5505/20-Ce/an\",\"signal\":\"-84\"},{\"address\":\"24:5A:4C:E0:7E:9D\",\"ssid\":\"alba\",\"channel\":\"5555/20-eC/an\",\"signal\":\"-72\"},{\"address\":\"C4:AD:34:7E:C6:15\",\"ssid\":\"MANGKUBUMI____A\",\"channel\":\"5560/20-Ce/an\",\"signal\":\"-80\"},{\"address\":\"2C:C8:1B:BA:96:BD\",\"ssid\":\"MMS-FJR\",\"channel\":\"5600/20/an\",\"signal\":\"-81\"},{\"address\":\"C4:AD:34:C8:F1:BE\",\"ssid\":\"POP-SKS-SA1\",\"channel\":\"5640/20-Ceee/ac\",\"signal\":\"-81\"},{\"address\":\"04:DA:D2:83:A5:8E\",\"ssid\":\"seamless@wifi.id\",\"channel\":\"5745/20/an\",\"signal\":\"-64\"},{\"address\":\"64:D1:54:AE:F9:E0\",\"ssid\":\"\",\"channel\":\"5745/20/an\",\"signal\":\"-33\"},{\"address\":\"04:DA:D2:83:A5:8D\",\"ssid\":\"@wifi.id\",\"channel\":\"5745/20/an\",\"signal\":\"-64\"},{\"address\":\"F0:9F:C2:46:4D:BA\",\"ssid\":\"Penggalang\",\"channel\":\"5750/20/an\",\"signal\":\"-87\"},{\"address\":\"CC:2D:E0:D0:24:27\",\"ssid\":\"\",\"channel\":\"5800/20/an\",\"signal\":\"-48\"},{\"address\":\"F4:92:BF:4C:F0:91\",\"ssid\":\"Sekctoral_CleoNet\",\"channel\":\"5825/20/an\",\"signal\":\"-77\"}]','2022-01-18 05:26:40',5),('ITN Radar Site panca Warna','[{\"address\":\"B8:69:F4:44:9D:9B\",\"ssid\":\"GPM SEC-UTARA\",\"channel\":\"5745/20/an\",\"signal\":\"-61\"},{\"address\":\"B4:FB:E4:A2:40:56\",\"ssid\":\"GPM_TO_SIDOBASUKI\",\"channel\":\"5775/20/an\",\"signal\":\"-86\"}]','2022-01-17 00:10:43',3),('ITN Radar site Tunas jaya','[{\"address\":\"64:D1:54:95:E2:0F\",\"ssid\":\"SMD to LK\",\"channel\":\"5735/20/an\",\"signal\":\"-35\"},{\"address\":\"64:D1:54:59:DB:3B\",\"ssid\":\"ScorpionNet to U\",\"channel\":\"5765/20/an\",\"signal\":\"-54\"},{\"address\":\"F0:9F:C2:9E:7B:2A\",\"ssid\":\"Scp to Ksn\",\"channel\":\"5795/20/an\",\"signal\":\"-64\"},{\"address\":\"44:D9:E7:0C:05:04\",\"ssid\":\"\",\"channel\":\"5825/20/an\",\"signal\":\"-41\"}]','2022-01-18 05:33:43',2),('Zitline Radar Site Pramuka','[{\"address\":\"B8:69:F4:D1:C2:A9\",\"ssid\":\"AP LDM WAY GALUH\",\"channel\":\"5260/20/an/DP\",\"signal\":\"-61\"},{\"address\":\"74:83:C2:88:79:67\",\"ssid\":\"@zitline_office\",\"channel\":\"5745/20-Ce/ac\",\"signal\":\"-78\"},{\"address\":\"F0:9F:C2:94:B4:32\",\"ssid\":\"apz-sec-prm-04\",\"channel\":\"5745/20/an\",\"signal\":\"-60\"},{\"address\":\"76:83:C2:88:79:67\",\"ssid\":\"@zitline_guest\",\"channel\":\"5745/20-Ce/ac\",\"signal\":\"-78\"},{\"address\":\"86:83:C2:88:79:67\",\"ssid\":\"\",\"channel\":\"5745/20-Ce/ac\",\"signal\":\"-76\"},{\"address\":\"00:19:3B:17:1D:FE\",\"ssid\":\"BDLUNLLW04\",\"channel\":\"5805/20/ac\",\"signal\":\"-81\"}]','2022-01-18 05:38:07',1);
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(1) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('admin','$2a$10$fZtUbJhA0.LJ1wbcYnqF1.tPrEFR43mMfyp7DXWYyBIFXjMYylmWW',1),('user','$2a$10$jvCxOudZ1ZYUXH/fj0KJhO7xlTAokysR2wyr.KVJ3krEuvciTIA3K',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'wlsc'
--

--
-- Dumping routines for database 'wlsc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-18 12:38:29
