const express = require('express');
const cors = require('cors');
const http = require('http');
// const https = require('https');
const mysql = require('mysql');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

require('dotenv').config();

const pool = mysql.createPool({
    connectionLimit : 100,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
});

const app = express();
const PORT = process.env.PORT;
// const PORT_HTTPS = process.env.PORT_HTTPS;

const httpServer = http.createServer(app);
// const httpsServer = https.createServer({
//    key: fs.readFileSync(process.env.PRIVKEY),
//    cert: fs.readFileSync(process.env.CERT),
// }, app);

app.use(cors());
app.use(express.json({ limit: '200mb' }));
app.use(express.urlencoded({ limit: '200mb', extended: false }));

app.get('/kabupaten', (req, res) => {
    pool.query(`SELECT * FROM kabupaten`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(200).json(result);
    });
});

app.post('/kabupaten', (req, res) => {
    pool.query(`INSERT INTO kabupaten (nama) VALUES ("${req.body.nama_kabupaten}")`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(201).json("Berhasil");
    });
});

app.get('/users', (req, res) => {
    pool.query(`SELECT * FROM users WHERE username != "admin"`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(200).json(result);
    });
});

app.post('/users', (req, res) => {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.username, salt);
    pool.query(`INSERT INTO users (username, password, role) VALUES ("${req.body.username}", "${hash}", "${req.body.role}")`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(201).json("Berhasil");
    });
});

app.put('/users', (req, res) => {
    pool.query(`
        UPDATE users SET
            role = "${req.body.role}"
        WHERE username = "${req.body.username}"
    `, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(201).json("Updated");
    });
});

app.delete('/users/:username', (req, res) => {
    pool.query(`
        DELETE FROM users WHERE username = "${req.params.username}"
    `, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(201).json("Deleted");
    });
});

app.get('/identity', (req, res) => {
    pool.query(`SELECT a.*, b.nama AS nama_kabupaten FROM results a LEFT JOIN kabupaten b ON a.id_kabupaten = b.id`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(200).json(result);
    });
});

app.get('/identity/:id_kabupaten', (req, res) => {
    pool.query(`SELECT identity FROM results WHERE id_kabupaten = "${req.params.id_kabupaten}"`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(200).json(result);
    });
});

app.post('/identity', (req, res) => {
    pool.query(`
        INSERT INTO results (identity, id_kabupaten, ipaddress, koordinat)
        VALUES ("${req.body.identity}", "${req.body.id_kabupaten}", "${req.body.ipaddress}", "${req.body.koordinat}")
    `, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(201).json("Berhasil");
    });
});

app.put('/identity', (req, res) => {
    pool.query(`
        UPDATE results SET
            ipaddress = "${req.body.ipaddress}",
            koordinat = "${req.body.koordinat}"
        WHERE identity = "${req.body.identity}"
    `, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        };

        res.status(201).json("Updated");
    });
});

app.get('/scan-result/tanggal/:tanggal/:identity', (req, res) => {
    const query = `
        SELECT
            scanResult,
            identity,
            ipaddress,
            koordinat,
            DATE_FORMAT(tgl, '%Y-%m-%d %T') AS tgl
        FROM results2
        WHERE identity = '${req.params.identity}' && DATE_FORMAT(tgl, '%Y-%m-%d') = '${req.params.tanggal}'
    `;
    // console.log(query)
    pool.query(query, (error, result) => {
        if (error) return res.status(500).json(error.sqlMessage);
        // if (result.length === 0) return res.status(500).json("Identity Tidak Ditemukan");

        res.status(200).json({
            ...result[0],
            data: result.length !== 0 ? JSON.parse(result[0].scanResult) : [],
        });
    });
});

app.get('/scan-result/:identity', (req, res) => {
    pool.query(`
        SELECT
            scanResult,
            identity,
            ipaddress,
            koordinat,
            DATE_FORMAT(tgl, '%Y-%m-%d %T') AS tgl
        FROM results
        WHERE identity = '${req.params.identity}'
    `, (error, result) => {
        if (error) return res.status(500).json(error.sqlMessage);
        if (result.length === 0) return res.status(500).json("Identity Tidak Ditemukan");

        res.status(200).json({
            ...result[0],
            data: JSON.parse(result[0].scanResult),
        });
    });
});

app.get('/cektoken', (req, res) => {
    jwt.verify(req.headers.token, process.env.KEY, function(error, decoded) {
        if (error) return res.status(500).json(error);

        return res.status(200).json(decoded);
    });
});

app.post('/204bd3i2rmD0EMq', (req, res) => {
    const data = JSON.parse(req.body.result);
    pool.query(`UPDATE results SET scanResult = '${JSON.stringify(data['scan-result'])}' WHERE identity = '${data.identity}'`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(200).json("Error");
        }

        res.status(200).json("Updated");
    });
    // pool.query(`SELECT * FROM results WHERE identity = '${data.identity}'`, (error, result) => {
    //     if (error) {
    //         console.log(error)
    //         return res.status(200).json("Error");
    //     }

    //     if (result.length === 0) {
    //         pool.query(`INSERT INTO results (identity, scanResult) VALUES ('${data.identity}', '${JSON.stringify(data['scan-result'])}')`, (error, result) => {
    //             if (error) {
    //                 console.log(error)
    //                 return res.status(200).json("Error");
    //             }
        
    //             res.status(200).json("Berhasil");
    //         });
    //     } else {
            
    //     }
    // });
});

app.post('/login', (req, res) => {
    const { username, password } = req.body;

    pool.query(`SELECT * FROM users WHERE username = '${username}'`, (error, result) => {
        if (error) {
            console.log(error)
            return res.status(500).json(error.sqlMessage);
        }
        if (result.length === 0) return res.status(500).json("Username Tidak Ditemukan");

        const user = result[0];
        const isUser = bcrypt.compareSync(password, user.password);
        if (isUser) {
            jwt.sign({ username: user.username, role: user.role }, process.env.KEY, function(error, token) {
                if (error) return res.status(500).json(error);

                return res.status(201).json(token);
            });
        } else {
            return res.status(500).json("Password Salah");
        }
    });
});

httpServer.listen(PORT, function() { console.log('Http Running On Port', PORT); });
// httpsServer.listen(PORT_HTTPS, function () { console.log('Https Running On Port', PORT_HTTPS); });

module.exports = app;

// const salt = bcrypt.genSaltSync(10);
// const hash = bcrypt.hashSync("", salt);
// console.log("masuk===", hash);
// const test = bcrypt.compareSync("", hash);
// console.log("keluar===", test)

// const guguk = '{"identity":"Identity 2","scan-result":[{"address":"FC:A6:CD:C6:0F:C0","ssid":"Sharonjoanna","channel":"2412/20/gn","signal":"-68"},{"address":"90:55:DE:16:27:90","ssid":"Mirza","channel":"2437/20/gn","signal":"-69"},{"address":"6E:3B:6B:A0:A2:46","ssid":"xavier","channel":"2442/20-eC/gn","signal":"-74"},{"address":"6C:3B:6B:A0:A2:46","ssid":"haus","channel":"2442/20-eC/gn","signal":"-75"},{"address":"74:CC:39:90:BA:E0","ssid":"Work Together","channel":"2452/20/gn","signal":"-83"},{"address":"84:07:FA:26:9A:98","ssid":"jefridesy","channel":"2462/20/gn","signal":"-58"},{"address":"E8:01:8D:B5:C1:70","ssid":"DESPRO ORGANIZER","channel":"2462/20/gn","signal":"-83"},{"address":"9C:9D:7E:28:17:B4","ssid":"DESPRO ORGANIZER_plus","channel":"2462/20/gn","signal":"-89"}]}';
