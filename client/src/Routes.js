import React from "react";
import { HashRouter as Router, Routes as Switch, Route } from "react-router-dom";
import Login from "./Login";
import Main from "./Main";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" element={<Login />} />
        <Route path="/main" element={<Main />} />
      </Switch>
    </Router>
  )
};

export default Routes;