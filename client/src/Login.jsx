import React from 'react';
import { Navigate } from 'react-router-dom';
import { Input, Button, notification, Form } from "antd";
import axios from "axios";
import { icon } from "./utils";

const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class Login extends React.Component {
	
	state = {
        loading: false,
        redirect: false,
    }
    
    saveLogin = (values) => {
        this.setState({ loading: true }, () => {
            api.post(`/login`, {
                username: values.username,
                password: values.password,
            })
            .then(result => {
                api.defaults.headers['token'] = result.data;
				localStorage.setItem('token', result.data);
				this.setState({ redirect: true });
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });
    }
    
    render() {
		return (
			<>
                <div className="p-0 w-full h-screen lg:p-3">
					<div
						className={`
                            h-full
                            bg-cover
                            bg-no-repeat
                            bg-center
                            p-10
                        `}
						style={{ backgroundImage: `url(${require('./assets/imgs/background.jpg')})` }}
					>
						<div className="flex justify-between">
							<div
								className="bg-white rounded-full w-24 h-24 block bg-contain bg-no-repeat bg-center p-2"
								style={{ backgroundImage: `url(${require('./assets/imgs/logo.png')})` }}
							></div>
							<div className="text-white">
								<div className="text-5xl"><b>APJII</b></div>
								<div>Radar Lampung</div>
							</div>
							{/* <div className="text-gray-600">
								<div>RS. Pertamedika Ummi Rosnati</div>
								<div>Telp. 08xx xxxx xxxx</div>
							</div> */}
						</div>
						<div className="flex items-center justify-end">
							<div className="p-5 bg-white rounded-lg w-64">
								<h1 className="mb-3 text-xl">Silahkan Masuk</h1>
								<Form
									onFinish={this.saveLogin}
									initialValues={{
										username: '',
										password: '',
									}}
								>
									<Form.Item
										name="username"
										rules={[
											{
												required: true,
												message: "Username Kosong!",
											},
										]}
									><Input placeholder="username" prefix={icon("MailOutlined")} /></Form.Item>
									<Form.Item
										name="password"
										rules={[
											{
												required: true,
												message: 'Password Kosong!',
											},
										]}
									><Input.Password placeholder="Password" prefix={icon("KeyOutlined")} /></Form.Item>
									<Form.Item>
										<Button
											className="mb-3 w-full text-white h-10 p-2 rounded bg-blue-600"
											type="text"
											htmlType="submit"
											loading={this.state.loading}
										>Login</Button>
									</Form.Item>
								</Form>
								<p className="text-sm text-gray-600">
									&copy; 2022 Verd IT Solution
								</p>
							</div>
						</div>
					</div>
				</div>

                {
					this.state.redirect ?
						<Navigate to={`/main`} />
					: null
				}
            </>
		);
	}

}

export default Login;
