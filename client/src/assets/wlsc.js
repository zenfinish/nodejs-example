const wlsc = (identity) => `#######
# interface wlan scanner exporter
# script by @ericksetiawan 2022
# use only with permission
###

:local newSysId "${identity}";

/file remove [find name="wlsc.rsc"];
:put "Importing Scripts";
/system script;
job remove [find script="wlsc"];
remove [find name="wlsc"];
add name="wlsc" policy=read,write,policy,test,password,sniff source={
:local iface "wlan1";
:local apiUrl "http://103.52.61.77:3008/204bd3i2rmD0EMq";

:do {
  :global urlEncode do={
    :local urlstring $1; :local urlEncoded;
    :for i from=0 to=([:len $urlstring] - 1) do={
      :local char [:pick $urlstring $i];
      :if ($char = "&") do={ :set $char "%26"; }
      :if ($char = "*") do={ :set $char "%2A"; }
      :if ($char = " ") do={ :set $char "%20"; }
      :if ($char = "?") do={ :set $char "%3F"; }
      :if ($char = "/") do={ :set $char "%2F"; }
      :if ($char = "'") do={ :set $char ""; }
      :set urlEncoded ($urlEncoded . $char);
    }
    :return $urlEncoded;
  }
  
  :log info message="running wlan scan";
  :local buf "";
  :local bgScan "no";
  :if ([/interface get $iface running]) do={ :set $bgScan "yes"; };
  /interface wireless scan $iface rounds=1 save-file=("scan-res-".$iface) background=$bgScan;
  :local content [/file get [/file find name=("scan-res-".$iface)] contents] ;
  :local contentLen [ :len $content ] ;
  /file remove [find name="scan-res-wlan1"];

  :local lineEnd 0; :local line ""; :local lastEnd 0;

  :set $buf ("{\"identity\":\"".[/system identity get name]."\",\"scan-result\":[");
  :while ($lastEnd < $contentLen) do={
    :set lineEnd [:find $content "\n" $lastEnd ] ;
    :set line [:pick $content $lastEnd $lineEnd] ;
    :set lastEnd ( $lineEnd + 1) ;

    :local tmpArray [:toarray $line] ;
    :if ( [:pick $tmpArray 0] != "" ) do={
      #:put ($tmpArray);
      :set $buf ($buf."{\""."address"."\":\"".($tmpArray->0)."\",");
      :set $buf ($buf."\""."ssid"."\":\"".[$urlEncode ($tmpArray->1)]."\",");
      :set $buf ($buf."\""."channel"."\":\"".[$urlEncode ($tmpArray->2)]."\",");
      :set $buf ($buf."\""."signal"."\":\"".($tmpArray->3)."\"}");
      :if ($lastEnd < $contentLen) do={ :set $buf ($buf.","); };
    };
  };
  :set $buf ($buf."]}");

  :local postdata ("result=".$buf);
  :put ($apiUrl."\?".$postdata); #debug
  :if ($apiUrl !="") do={ /tool fetch url=$apiUrl http-method=post http-data=$postdata keep-result=no; :log info message="scan result data sent";};
} on-error={
  :put "scan script failed to run";
  :log error message="scan script failed to run";
}
};

:put "Importing Scheduler";
/system scheduler;
remove [find name="wlsc"];
add disabled=no interval=5m name="wlsc" on-event={:if ([:len [/system script job find script=wlsc]] = 0 ) do={/system script run wlsc;}} policy=read,write,policy,test,password,sniff start-time=startup;

remove [find name="autorb"];
add disabled=no interval=1d name="autorb" on-event={/system reboot;} policy=reboot start-time=00:00:00;


:put "Changing System Identity";
/system identity set name=$newSysId;`;

export default wlsc;