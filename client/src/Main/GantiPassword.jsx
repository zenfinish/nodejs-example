import React from 'react';
import { notification, Input, Button } from "antd";
import axios from "axios";

const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class GantiPassword extends React.Component {
    
    state = {
        password_lama: "",
        password_baru: "",
        ulangi_password: "",

        loading: false,
    }

    simpan = () => {
        api.post(`/users/password`, {
            password_lama: this.state.password_lama,
            password_baru: this.state.password_baru,
            ulangi_password: this.state.ulangi_password,
        })
        .then(result => {
            localStorage.clear();
            window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
        })
        .catch(error => {
            notification.error({
                message: 'Error',
                description: error.response.data,
            });
            this.setState({ loading: false });
        });
    }
    
    render() {
		return (
            <>
                <div className="grid grid-cols-3 gap-1">
                    <div>
                        <label>Password Lama</label>
                        <Input
                            type="password"
                            onChange={(e) => this.setState({ password_lama: e.target.value })}
                            value={this.state.password_lama}
                        />
                    </div>
                    <div>
                        <label>Password Baru</label>
                        <Input
                            type="password"
                            onChange={(e) => this.setState({ password_baru: e.target.value })}
                            value={this.state.password_baru}
                        />
                    </div>
                    <div>
                        <label>Ulangi Password Baru</label>
                        <Input
                            type="password"
                            onChange={(e) => this.setState({ ulangi_password: e.target.value })}
                            value={this.state.ulangi_password}
                        />
                    </div>
                </div>
                <Button onClick={this.simpan}>Update</Button>
            </>
		);
	}

}

export default GantiPassword;
