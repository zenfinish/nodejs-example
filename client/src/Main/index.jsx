import React from 'react';
import { Button } from "antd";
import axios from "axios";
import MainView from "./MainView";
import TambahRadar from "./TambahRadar";
import TambahKabupaten from "./TambahKabupaten";
import ManajemenUser from './ManajemenUser';
import GantiPassword from './GantiPassword';

const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class Main extends React.Component {
	
	state = {
        data: {
            username: "",
            role: null,
        },
        menu: 1,
    }
    
    componentDidMount() {
        this.checkToken();
    }

    checkToken = () => {
        api.get(`/cektoken`, {
            headers: { token: localStorage.getItem('token') }
        })
        .then(result => {
            this.setState({ data: result.data });
        })
        .catch(error => {
            window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
        });
    }
    
    render() {
		return (
			<div className="p-5">
                <div className="mb-1 flex gap-1">
                    <Button
                        onClick={() => {
                            localStorage.clear();
                            window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
                        }}
                    >Logout</Button>
                    <Button
                        onClick={() => this.setState({ menu: 1 })}
                    >Home</Button>
                    {
                        this.state.data.role === 1 ?
                            <>
                                <Button
                                    onClick={() => this.setState({ menu: 2 })}
                                >Tambah Radar</Button>
                                <Button
                                    onClick={() => this.setState({ menu: 3 })}
                                >Tambah Kabupaten</Button>
                                <Button
                                    onClick={() => this.setState({ menu: 4 })}
                                >Manajemen User</Button>
                            </>
                        : null
                    }
                    <Button
                        onClick={() => this.setState({ menu: 5 })}
                    >Ganti Password</Button>
                </div>
                <div className="bg-slate-100 p-5">
                    {
                        this.state.menu === 1 ?
                            <MainView />
                        : null
                    }
                    {
                        this.state.menu === 2 ?
                            <TambahRadar />
                        : null
                    }
                    {
                        this.state.menu === 3 ?
                            <TambahKabupaten />
                        : null
                    }
                    {
                        this.state.menu === 4 ?
                            <ManajemenUser />
                        : null
                    }
                    {
                        this.state.menu === 5 ?
                            <GantiPassword />
                        : null
                    }
                </div>
            </div>
		);
	}

}

export default Main;
