import React from 'react';
import { notification, Input, Button, Modal, Radio } from "antd";
import axios from "axios";

const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class ModalForm extends React.Component {
    
    state = {
        username: null,
        role: null,

        loading: false,
    }

    componentDidMount() {
        if (this.props.modeModal === "edit") {
            this.setState(this.props.dataAktif);
        };
    }

    simpan = () => {
        api.post(`/users`, {
            username: this.state.username,
            role: this.state.role,
        })
        .then(result => {
            this.props.closeRefresh();
        })
        .catch(error => {
            notification.error({
                message: 'Error',
                description: error.response.data,
            });
            this.setState({ loading: false });
        });
    }

    update = () => {
        api.put(`/users`, {
            username: this.state.username,
            role: this.state.role,
        })
        .then(result => {
            this.props.closeRefresh();
        })
        .catch(error => {
            notification.error({
                message: 'Error',
                description: error.response.data,
            });
            this.setState({ loading: false });
        });
    }
    
    render() {
		const { modeModal, dataAktif} = this.props;
        return (
            <>
                <Modal
                    title={`${modeModal === "add" ? "Tambah" : modeModal === "edit" ? "Edit" : ""} User ${modeModal === "edit" ? dataAktif.username  : ""}`}
                    visible={true}
                    onCancel={() => this.props.close()}
                    footer={[
                        <div key="update">
                            {
                                modeModal === "add" ? <Button onClick={this.simpan}>Tambah</Button> :
                                modeModal === "edit" ? <Button onClick={this.update}>Update</Button> :
                                null
                            }
                        </div>
                    ]}
                >
                    <table>
                        <tbody>
                            <tr>
                                <td>Username</td>
                                <td>:</td>
                                <td>
                                    <Input
                                        type="text"
                                        onChange={(e) => this.setState({ username: e.target.value })}
                                        value={this.state.username}
                                        className="w-full"
                                        disabled={this.props.modeModal === "edit" ? true : false}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>Role</td>
                                <td>:</td>
                                <td>
                                    <Radio.Group
                                        onChange={(e) => this.setState({ role: e.target.value })}
                                        value={this.state.role}
                                    >
                                        <Radio value={1}>Admin</Radio>
                                        <Radio value={2}>User</Radio>
                                    </Radio.Group>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </Modal>
            </>
		);
	}

}

export default ModalForm;
