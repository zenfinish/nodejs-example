import React from 'react';
import { notification, Table, Button } from "antd";
import axios from "axios";
import ModalForm from './ModalForm';

const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class index extends React.Component {
    
    state = {
        username: null,
        role: null,

        data: [],
        aktif: {
            username: null,
            role: null,
        },

        loading: false,

        openModal: false,
        modeModal: null,
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData = () => {
        this.setState({ loading: true }, () => {
            api.get(`/users`)
            .then(result => {
                this.setState({ data: result.data, loading: false });
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });
    }

    hapus = (username) => {
        this.setState({ loading: true }, () => {
            api.delete(`/users/${username}`)
            .then(result => {
                this.fetchData();
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });
    }
    
    render() {
		return (
            <>
                <Table
                    title={() => (
                        <Button onClick={() => this.setState({ modeModal: "add", openModal: true })}>Tambah User</Button>
                    )}
                    dataSource={this.state.data}
                    columns={[
                        {
                            title: 'Username',
                            dataIndex: 'username',
                        },
                        {
                            title: 'Role',
                            dataIndex: 'role',
                            render: (text) => text === 1 ? "Admin" : text === 2 ? "User" : ""
                        },
                        {
                            title: 'Action',
                            render: (text, record) => (
                                <>
                                    <Button onClick={() => this.setState({ aktif: record, modeModal: "edit", openModal: true })}>Edit</Button>
                                    <Button onClick={() => this.hapus(record.username)}>Hapus</Button>
                                </>
                            )
                        },
                    ]}
                    rowKey={(row) => row.username}
                    loading={this.state.loading}
                />
                {
                    this.state.openModal ?
                        <ModalForm
                            close={() => this.setState({ openModal: false })}
                            closeRefresh={() => this.setState({ openModal: false }, () => this.fetchData())}
                            modeModal={this.state.modeModal}
                            dataAktif={this.state.aktif}
                        />
                    : null
                }
            </>
		);
	}

}

export default index;
