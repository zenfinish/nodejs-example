import React from 'react';
import { notification, Table, Input, Button, Select, Modal } from "antd";
import axios from "axios";
import wlsc from '../assets/wlsc';

const { Option } = Select;
const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class TambahRadar extends React.Component {
    
    state = {
        identity: "",
        id_kabupaten: null,
        ipaddress: null,
        koordinat: null,

        dataKabupaten: [],
        data: [],

        aktif: {
            identity: null,
            ipaddress: null,
            koordinat: null,
        },

        loading: false,
        openEdit: false,
    }

    componentDidMount() {
        this.fetchData();
        this.fetchKabupaten();
    }

    fetchData = () => {
        this.setState({ loading: true }, () => {
            api.get(`/identity`)
            .then(result => {
                this.setState({ data: result.data, loading: false });
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });
    }

    fetchKabupaten = () => {
        api.get(`/kabupaten`)
        .then(result => {
            this.setState({ dataKabupaten: result.data, loading: false });
        })
        .catch(error => {
            notification.error({
                message: 'Error',
                description: error.response.data,
            });
            this.setState({ loading: false });
        });
    }

    simpan = () => {
        this.setState({ loading: true }, () => {
            api.post(`/identity`, {
                identity: this.state.identity,
                id_kabupaten: this.state.id_kabupaten,
                ipaddress: this.state.ipaddress,
                koordinat: this.state.koordinat,
            })
            .then(result => {
                this.fetchData();
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });
    }

    download = (data) => {
        const element = document.createElement("a");
        const file = new Blob([wlsc(data.identity)], { type: 'text/plain' });
        element.href = URL.createObjectURL(file);
        element.download = `${data.identity}.src`;
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
    }
    
    render() {
		return (
            <>
                <Table
                    title={() => (
                        <>
                            <div className="grid grid-cols-3 gap-1">
                                <div>
                                    <label>Identity</label>
                                    <Input
                                        type="text"
                                        onChange={(e) => this.setState({ identity: e.target.value })}
                                        value={this.state.identity}
                                        className="w-full"
                                    />
                                </div>
                                <div>
                                    <label>Kabupaten</label>
                                    <Select
                                        value={this.state.id_kabupaten}
                                        onChange={(value) => this.setState({ id_kabupaten: value })}
                                        className="w-full"
                                    >
                                        {
                                            this.state.dataKabupaten.map((row, i) => (
                                                <Option value={row.id} key={i}>{row.nama}</Option>
                                            ))
                                        }
                                    </Select>
                                </div>
                                <div>
                                    <label>IP Address:PORT</label>
                                    <Input
                                        type="text"
                                        onChange={(e) => this.setState({ ipaddress: e.target.value })}
                                        value={this.state.ipaddress}
                                        className="w-full"
                                    />
                                </div>
                                <div>
                                    <label>Koordinat</label>
                                    <Input
                                        type="text"
                                        onChange={(e) => this.setState({ koordinat: e.target.value })}
                                        value={this.state.koordinat}
                                        className="w-full"
                                    />
                                </div>
                            </div>
                            <Button onClick={this.simpan} className="mt-1">Simpan</Button>
                        </>
                    )}
                    dataSource={this.state.data}
                    columns={[
                        {
                            title: 'Identity',
                            dataIndex: 'identity',
                        },
                        {
                            title: 'Kabupaten',
                            dataIndex: 'nama_kabupaten',
                        },
                        {
                            title: 'IP Address:PORT',
                            dataIndex: 'ipaddress',
                        },
                        {
                            title: 'Koordinat',
                            dataIndex: 'koordinat',
                        },
                        {
                            title: 'Action',
                            render: (record, text, index) => (
                                <div className="flex gap-1">
                                    <Button onClick={() => this.setState({ aktif: record, openEdit: true })}>Edit</Button>
                                    <Button onClick={() => this.download(record)}>Download Script</Button>
                                </div>
                            ),
                        },
                    ]}
                    rowKey={(row) => row.identity}
                    loading={this.state.loading}
                />
                <Modal
                    title={`Edit Radar ${this.state.aktif.identity}`}
                    visible={this.state.openEdit}
                    onCancel={() => {
                        this.setState({ openEdit: false });
                    }}
                    footer={[
                        <div key="update">
                            <Button onClick={() => {
                                api.put(`/identity`, this.state.aktif)
                                .then(result => {
                                    this.setState({ openEdit: false });
                                    this.fetchData();
                                })
                                .catch(error => {
                                    notification.error({
                                        message: 'Error',
                                        description: error.response.data,
                                    });
                                });
                            }}>Update</Button>
                        </div>
                    ]}
                >
                    <table>
                        <tbody>
                            <tr>
                                <td>IP Address:PORT</td>
                                <td>:</td>
                                <td>
                                    <Input
                                        type="text"
                                        onChange={(e) => this.setState({ aktif: { ...this.state.aktif, ipaddress: e.target.value } })}
                                        value={this.state.aktif.ipaddress}
                                        className="w-full"
                                    />    
                                </td>
                            </tr>
                            <tr>
                                <td>Koordinat</td>
                                <td>:</td>
                                <td>
                                    <Input
                                        type="text"
                                        onChange={(e) => this.setState({ aktif: { ...this.state.aktif, koordinat: e.target.value } })}
                                        value={this.state.aktif.koordinat}
                                        className="w-full"
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </Modal>
            </>
		);
	}

}

export default TambahRadar;
