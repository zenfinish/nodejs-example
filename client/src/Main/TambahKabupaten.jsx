import React from 'react';
import { notification, Table, Input, Button } from "antd";
import axios from "axios";

const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class TambahKabupaten extends React.Component {
    
    state = {
        nama_kabupaten: "",
        data: [],
        loading: false,
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData = () => {
        api.get(`/kabupaten`)
        .then(result => {
            this.setState({ data: result.data, loading: false });
        })
        .catch(error => {
            notification.error({
                message: 'Error',
                description: error.response.data,
            });
            this.setState({ loading: false });
        });
    }

    simpan = () => {
        api.post(`/kabupaten`, {
            nama_kabupaten: this.state.nama_kabupaten
        })
        .then(result => {
            this.fetchData();
        })
        .catch(error => {
            notification.error({
                message: 'Error',
                description: error.response.data,
            });
            this.setState({ loading: false });
        });
    }
    
    render() {
		return (
            <>
                <Table
                    title={() => (
                        <div>
                            <span>Tambah Kabupaten: </span>
                            <Input
                                type="text"
                                onChange={(e) => this.setState({ nama_kabupaten: e.target.value })}
                                value={this.state.nama_kabupaten}
                                placeholder="Nama Kabupaten"
                            />
                            <Button onClick={this.simpan}>Simpan</Button>
                        </div>
                    )}
                    dataSource={this.state.data}
                    columns={[
                        {
                            title: 'ID',
                            dataIndex: 'id',
                        },
                        {
                            title: 'Nama',
                            dataIndex: 'nama',
                        },
                    ]}
                    rowKey={(row) => row.id}
                />
            </>
		);
	}

}

export default TambahKabupaten;
