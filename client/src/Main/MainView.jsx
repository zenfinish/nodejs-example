import React from 'react';
import { notification, Tree, Table, Tag, Modal, Input } from "antd";
import axios from "axios";
import tglIndo from '../helpers/tglIndo';

const api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

class MainView extends React.Component {
	
	state = {
        data: [],

        scanResult: {
            tgl: "",
            identity: "",
            kabupaten: "",
            ipaddress: "",
            koordinat: "",
            data: [],
        },

        identity: null,
        kabupaten: null,
        updateTerakhir: null,
        tanggal: null,

        loading: false,
        openModal: false,
    }
    
    componentDidMount() {
        this.fetchKabupaten();
    }
    
    fetchKabupaten = () => {
        this.setState({ loading: true }, () => {
            api.get(`/kabupaten`)
            .then(result => {
                const data = result.data.map((row) => { return { key: row.id, title: row.nama } });
                this.setState({ data, loading: false });
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });
    }

    fetchScanResult = (identity, kabupaten) =>
        this.setState({ loading: true }, () => {
            api.get(`/scan-result/${identity}`)
            .then(result => {
                this.setState({
                    scanResult: {
                        ...result.data,
                        kabupaten
                    },
                    updateTerakhir: result.data.tgl,
                    loading: false
                });
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });

    fetchTanggal = (identity, kabupaten, tanggal) =>
        this.setState({ loading: true }, () => {
            api.get(`/scan-result/tanggal/${tanggal}/${identity}`)
            .then(result => {
                this.setState({ scanResult: { ...result.data, kabupaten }, loading: false });
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: error.response.data,
                });
                this.setState({ loading: false });
            });
        });

    updateTreeData = (list, key, children) => {
        return list.map((node) => {
            if (node.key === key) {
                return { ...node, children };
            };
        
            if (node.children) {
                return { ...node, children: this.updateTreeData(node.children, key, children) };
            };
        
            return node;
        });
    }

    onLoadData = ({ key, children, title }) => {
        return new Promise((resolve, reject) => {
            if (children) {
                resolve();
                return;
            };
            api.get(`/identity/${key}`)
            .then(result => {
                const identity = result.data.map((row) => { return { key: row.identity, title: row.identity, isLeaf: true, kabupaten: title } }); 
                const data = this.updateTreeData(this.state.data, key, identity);
                this.setState({ data }, () => resolve());
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    onSelect = (selectedKeys, { selected, selectedNodes, node, event }) => {
        // console.log("selected===", selected)
        // console.log("selectedNodes===", selectedNodes)
        // console.log("node===", node)
        // console.log("event===", event)
        if (typeof node.key !== "number") {
            this.setState({ identity: node.key, kabupaten: node.kabupaten }, () => {
                this.fetchScanResult(node.key, node.kabupaten);
            });
        };
    }
    
    render() {
		return (
            <>
                <div className="grid grid-cols-4 gap-2">
                    <div>
                        <Tree
                            loadData={this.onLoadData}
                            treeData={this.state.data}
                            onSelect={this.onSelect}
                            titleRender={(nodeData) => typeof nodeData.key === "number" ? <span className="font-bold">{nodeData.title}</span> : nodeData.title}
                        />
                    </div>
                    <div className="col-span-3">
                        <Table
                            title={() => (
                                <div className="flex justify-between">
                                    <Input
                                        type="date"
                                        onChange={(e) => {
                                            if (e.target.value && e.target.value.length === 10) {
                                                this.fetchTanggal(this.state.identity, this.state.kabupaten, e.target.value);
                                            };
                                        }}
                                        style={{ width: '200px' }}
                                    />
                                    <button
                                        className="font-bold hover:bg-sky-700 hover:text-white hover:rounded px-1"
                                        onClick={() => {
                                            this.setState({ openModal: true });
                                        }}
                                    >{this.state.scanResult.identity}</button>
                                    <span className="font-bold">{this.state.scanResult.kabupaten}</span>
                                    <span className="font-bold">Update Terakhir: {tglIndo(this.state.updateTerakhir)}</span>
                                </div>
                            )}
                            dataSource={this.state.scanResult.data}
                            columns={[
                                {
                                    title: 'address',
                                    dataIndex: 'address',
                                },
                                {
                                    title: 'ssid',
                                    dataIndex: 'ssid',
                                },
                                {
                                    title: 'channel',
                                    dataIndex: 'channel',
                                    render: (text, record, index) => <Tag color={
                                        Number(record.channel.substring(0, 4)) >= 5725 && Number(record.channel.substring(0, 4)) <= 5825 ? "#87d068" :
                                        Number(record.channel.substring(0, 4)) >= 5500 && Number(record.channel.substring(0, 4)) <= 5695 ? "#f50" :
                                        "gold"
                                    }>{text}</Tag>
                                },
                                {
                                    title: 'signal',
                                    dataIndex: 'signal',
                                },
                            ]}
                            rowKey={(row) => row.address}
                            loading={this.state.loading}
                            pagination={false}
                        />
                    </div>
                </div>
                <Modal
                    title="Detail Radar"
                    visible={this.state.openModal}
                    onCancel={() => {
                        this.setState({ openModal: false });
                    }}
                >
                    <table>
                        <tbody>
                            <tr>
                                <td>Identity</td>
                                <td>:</td>
                                <td>{this.state.scanResult.identity}</td>
                            </tr>
                            <tr>
                                <td>IP Address:PORT</td>
                                <td>:</td>
                                <td>{this.state.scanResult.ipaddress}</td>
                            </tr>
                            <tr>
                                <td>Koordinat</td>
                                <td>:</td>
                                <td>{this.state.scanResult.koordinat}</td>
                            </tr>
                        </tbody>
                    </table>
                </Modal>
            </>
		);
	}

}

export default MainView;
